import React from 'react';

import styles from './NumberInput.module.css';

interface NumberInputProps {
    min?: number,
    max?: number,
    step?: number,
    value?: number,
    onChange?: (value: number) => void
}

const NumberInput : React.FunctionComponent<NumberInputProps> = (props: NumberInputProps) => {
    const min = props.min ? props.min : Number.MIN_VALUE;
    const max = props.max ? props.max : Number.MAX_VALUE;
    const step = props.step ? props.step : 1;
    const value = props.value ? props.value : 0;
    const onChange = props.onChange ? props.onChange : () => undefined;

    return (
        <div className={styles.NumberInput}>
            <button
                disabled={value === min}
                onClick={() => onChange(value - 1)}
            >-</button>
            <input
                type="number"
                min={min}
                max={max}
                step={step}
                value={value}
                onChange={(e) => onChange(e.target.valueAsNumber)}
            />
            <button
                disabled={value === max}
                onClick={() => onChange(value + 1)}
            >+</button>
        </div>
    );
};

export default NumberInput;
