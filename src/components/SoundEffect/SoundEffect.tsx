import React, { useEffect, useRef } from 'react';

interface SoundEffectProps {
    sources: Array<{src: string, type: string}>,
    play: boolean
}

const soundEffect = (props: SoundEffectProps) => {
    const audioRef = useRef<HTMLAudioElement>(null);

    useEffect(() => {
        const audioNode = audioRef.current;

        if (audioNode) {
            if (props.play) {
                audioNode.play();
            }
            else {
                audioNode.pause();
            }
        }
    }, [props.play]);

    return (
        <audio ref={audioRef}>
            {
                props.sources.map(source => <source src={source.src} type={source.type} />)
            }
        </audio>
    );
};

export default soundEffect;
