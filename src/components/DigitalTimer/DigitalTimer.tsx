import React, { useState, useEffect, CSSProperties } from 'react';
import styles from './DigitalTimer.module.css';

export interface DigitalTimerProps {
    seconds: number,
    style?: CSSProperties,
    start?: boolean,
    stop?: boolean,
    onEnd?: () => void,
    onTick?: (secondsLeft: number) => void
};

const pad = (number: number) => number < 10 ? '0' + number : '' + number;

const seconds2digits = (timer: number): string[] => {
    const minutes = Math.floor(timer / 60);
    const seconds = timer % 60;

    return [pad(minutes), pad(seconds)];
};

const timerBehavior = (
    secondsState: [number, React.Dispatch<React.SetStateAction<number>>],
    doStart: boolean,
    doStop: boolean,
    onTick?: Function,
    onEnd?: Function
) => {
    const [seconds, setSeconds] = secondsState;

    if (doStart) {
        if (seconds > 0 && !doStop) {

            const handle = window.setTimeout(() => {
                setSeconds(seconds - 1);
                (onTick ? onTick : () => null)(seconds - 1);
            }, 1000);

            return () => window.clearTimeout(handle);
        }
        else {
            (onEnd ? onEnd : () => null)();
        }
    }
}

const makeJsx = (seconds: number) => (
    seconds2digits(seconds)
        .map((part) => <span className={styles.digits}>{part}</span>)
        .map((styledPart, index, arr) => (
            <span key={index}>
                {styledPart}
                {
                    index !== arr.length - 1
                        ? <span className={styles.separator}>:</span>
                        : null
                }
            </span>
        ))
);

const digitalTimer = (props: DigitalTimerProps) => {
    const [seconds, setSeconds] = useState(props.seconds);

    useEffect(
        () => timerBehavior([seconds, setSeconds], !!props.start, !!props.stop, props.onTick, props.onEnd),
        [seconds]
    );

    return (
        <div style={props.style}>
            <div className={styles.digitalTimer}>
                {makeJsx(seconds)}
            </div>
        </div>
    );
};

export default digitalTimer;
