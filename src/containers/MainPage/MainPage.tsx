import React, { useState } from 'react';
import { withRouter } from 'react-router';

import Button from "@material-ui/core/Button";
import { Typography, Input, InputAdornment, FormControl, Checkbox, FormGroup, FormControlLabel, FormLabel, Grid } from '@material-ui/core';
import { PlayArrow } from '@material-ui/icons';

const mainPage = (props: any) => {
    const [seconds, setSeconds] = useState(60);
    const [fullscreen, setFullscreen] = useState(false);
    const [alarm, setAlarm] = useState(true);

    const gotoRunPage = () => props.history.push(`/run?time=${seconds}&alarm=${alarm}`);

    const startTimer = () => {
        if (fullscreen) {
            document.getElementsByTagName('html')[0].requestFullscreen()
                .then(gotoRunPage)
                .catch(() => alert('Could not enter fullscreen...'));
        }
        else {
            gotoRunPage();
        }
    }

    return (
        <div>
            <Typography style={{ textAlign: 'center', marginBottom: '.5em' }} variant="h4" component="h2">
                Set up a new timebox
            </Typography>

            <Grid container direction="column" alignItems="center" spacing={16}>
                <Grid item>
                    <FormControl>
                        <FormLabel>1. Set up the minutes for the timer to run</FormLabel>
                            <Input
                                type="number"
                                inputProps={{ min: .25, step: .25, "aria-label": "Minutes to run" }}
                                value={seconds / 60}
                                onChange={(e) => setSeconds(parseFloat(e.target.value) * 60)}
                                endAdornment={<InputAdornment position="end">min</InputAdornment>}
                            />
                    </FormControl>
                </Grid>

                <Grid item>
                    <FormControl>
                        <FormLabel>2. If you'd like to, you can set some additional options</FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                label="Run the timebox in fullscreen"
                                control={
                                    <Checkbox id="chkboxFullscreen"  checked={fullscreen} onChange={() => setFullscreen(!fullscreen)} />
                                }
                            />
                            <FormControlLabel
                                label="Play alarm sound when timebox ends"
                                control={
                                    <Checkbox id="chkboxAlarm" checked={alarm} onChange={() => setAlarm(!alarm)} />
                                }
                            />
                        </FormGroup>
                    </FormControl>
                </Grid>
                
                <Grid item>
                    <FormControl>
                        <FormLabel>3. Start the timer when you're ready</FormLabel>
                        <Button
                            onClick={startTimer}
                            variant="contained"
                            color="primary"
                        >
                            <PlayArrow />
                            Start!
                        </Button>
                    </FormControl>
                </Grid>
            </Grid>
        </div>
    );
}

export default withRouter(mainPage);
