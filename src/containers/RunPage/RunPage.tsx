import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';

// @ts-ignore
import FitText from '@kennethormandy/react-fittext';

import DigitalTimer from "../../components/DigitalTimer/DigitalTimer";

// @ts-ignore
import * as alarmSound from './alarm_digital.mp3';
import SoundEffect from '../../components/SoundEffect/SoundEffect';
import { Button } from '@material-ui/core';
import { Stop as StopIcon } from '@material-ui/icons';

const DEFAULT_TITLE = document.title;

const resetTitle = () => {
    document.title = DEFAULT_TITLE;
    return;
};

const exit = (props: any) => {
    document.exitFullscreen();
    props.history.push('/');
}

const runPage = (props: any) => {
    const params = new URLSearchParams(props.location.search);
    const time = parseInt(params.get('time') || '');
    const alarmEnabled = params.get('alarm') === 'true';

    const [alarmPlaying, setAlarmPlaying] = useState(false);

    document.title = time + 's Timebox - ' + DEFAULT_TITLE;
    
    if (isNaN(time) || time < 0) {
        return <p>Sorry, <code>{JSON.stringify(params.get('time'))}</code> could not be interpreted as seconds.</p>
    }

    useEffect(() => resetTitle, []);

    return (
        <div style={{ height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            <FitText compressor={.33}>
                <DigitalTimer start
                    seconds={ time }
                    onEnd={() => setAlarmPlaying(true)}
                />
                {
                    alarmPlaying && alarmEnabled
                        ? <SoundEffect sources={[{src: alarmSound, type: 'audio/mpeg'}]} play />
                        : null
                }
            </FitText>
            <Button
                variant="fab"
                color="secondary"
                style={{ position: 'fixed', bottom: '2em', right: '2em' }}
                onClick={() => exit(props)}
            >
                <StopIcon />
            </Button>
        </div>
    );
};

export default withRouter(runPage);
