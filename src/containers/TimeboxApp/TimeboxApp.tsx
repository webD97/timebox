import React from 'react';
import { Switch, Route } from 'react-router-dom';
import RunPage from '../RunPage/RunPage';
import MainPage from '../MainPage/MainPage';

import { Paper, CssBaseline, AppBar, Typography, Toolbar, Link, IconButton, MuiThemeProvider, createMuiTheme, } from '@material-ui/core';
import { Code } from '@material-ui/icons';

import { repository as REPOSITORY_URL } from '../../../package.json';

const app = (props: any) => {
    return (
      <MuiThemeProvider theme={createMuiTheme({ palette: { type: 'dark' } })}>
        <CssBaseline />
        <AppBar position="fixed" style={{ flexGrow: 0 }}>
          <Toolbar>
            <Typography
              variant="h5"
              component="h1"
              color="inherit"
              style={{ flexGrow: 1 }}
              onClick={() => props.history.push('/')}
            >
              Timebox App
            </Typography>
            <Link href={REPOSITORY_URL} target="_blank" rel="noopener" color="inherit">
              <IconButton color="inherit" title="View source on GitHub"><Code /></IconButton>
            </Link>
          </Toolbar>
        </AppBar>
        <Paper style={{ flexGrow: 1, marginTop: '64px', overflow: 'auto', padding: '1em', paddingTop: '2.5em' }}>
          <Switch>
            <Route path="/run" component={RunPage} />
            <Route path="/" exact component={MainPage} />
          </Switch>
        </Paper>
      </MuiThemeProvider>
    );
}

export default app;
